package number2;

import java.util.ArrayList;

public class Calling {

	public void horn(Car car){
		car.horn();}
	
	public void horn(Truck truck){
		truck.horn();}
	
	public void horn(Vehicle vehicle){
	vehicle.horn();}
	
	public static void main(String[] args) {
		Car car = new Car();
		Truck truck = new Truck();
		ArrayList<Vehicle> vehicles = new  ArrayList<Vehicle>();
		vehicles.add(car);
		vehicles.add(truck);
		//Polymorphism
		System.out.println(">> Polymorphism");
		for (Vehicle vehicle : vehicles){
			System.out.println(vehicle.horn());			
		}
		System.out.println(">> Call by Object");
		System.out.println(car.horn());
		System.out.println(truck.horn());
		
		/* ��������ض�觼�ҹ�����Ẻ�������ԫ�������� super class �����  abstract method 
		������ö���¡��  method horn �¡�����ҧẺ new object */
	}
}
