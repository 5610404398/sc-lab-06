package number3;
import number1.*;
import number2.*;

public class TestAccessModifier extends number1.Car {
	public static String a ="test public";
	private static  String b ="test private";
	protected static  String c ="test protected";
	public void change(int amount) {
		int balance = 2;
	}

	public static void main(String[] args) {
		TestAccessModifier2 t = new TestAccessModifier2();  // same package
		
		/** Test Access Modifier with same class  **/
		System.out.println(":: Test Access Modifier with same class ::");
		System.out.println(a);
		System.out.println(b);
		System.out.println(c); 
		
		/** Test Access Modifier with another package **/
		System.out.println("\n:: Test Access Modifier with another package ::");
		System.out.println(number2.Car.a);
		//System.out.println(number2.Car.b); >> Can't print because Access modifier is private 
		//System.out.println(number2.Car.c); >> Can't print because Access modifier is protected
		
		/** Test Access Modifier with same package **/
		System.out.println("\n:: Test Access Modifier with same package ::");
		System.out.println(t.a);
		//System.out.println(t.b); >> Can't print because Access modifier is private 
		System.out.println(t.c);
		
		/** Test Access Modifier with same package but both class are relation  **/
		System.out.println("\n:: Test Access Modifier with same package but both class are relation ::");
		System.out.println(number1.Car.a);
		//System.out.println(number1.Car.b); >> Can't print because Access modifier is private 
		System.out.println(number1.Car.c); 
		
	}
}